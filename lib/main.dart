import 'package:flutter/material.dart';

import 'item_screen1.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  static const appTitle = 'Drawer Demo';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      home: MyHomePage(title: appTitle),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState(title: title);
}

class _MyHomePageState extends State<MyHomePage> {
  _MyHomePageState({required this.title});
  final String title;
  Widget body = Center(
    child: Text('My Page'),
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: body,
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(child: Text('haikyuu'), 
              decoration: BoxDecoration(color: Colors.blue),
            ),
            ListTile(
              title: const Text('yuu'),
              onTap: (){
                setState(() {
                  body = Center(
                    child: Text('nishinoya'),
                  );
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('koshi'),
              onTap: (){
                setState(() {
                  body = Center(
                    child: Text('sugawara'),
                  );
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('shoyo'),
              onTap: (){
                setState(() {
                  body = Center(
                    child: Text('hinata'),
                  );
                });
                Navigator.pop(context);
              },
            ),
          ],
          ),
      ),
    );
  }
}
